package datecalc;

import datecalc.date.DateDifference;
import datecalc.date.MyDate;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DateDifferenceTest {
    @Test
    public void testGetYear() {
        MyDate date1 = new MyDate(10, 4, 1934);
        MyDate date2 = new MyDate(5, 3, 2017);

        assertEquals(82, new DateDifference(date1, date2).getYears());

        date2 = new MyDate(5, 5, 2017);
        assertEquals(83, new DateDifference(date1, date2).getYears());

        date2 = new MyDate(5, 5, 1934);
        assertEquals(0, new DateDifference(date1, date2).getYears());

        date1 = new MyDate(3, 8, 2008);
        date2 = new MyDate(28, 2, 2016);
        assertEquals(7, new DateDifference(date1, date2).getYears());

        date1 = new MyDate(3, 8, 2016);
        date2 = new MyDate(28, 2, 2016);
        assertEquals(0, new DateDifference(date1, date2).getYears());
    }

    @Test
    public void testGetMonths() {
        MyDate date1 = new MyDate(1, 6, 1934);
        MyDate date2 = new MyDate(7, 5, 2017);

        assertEquals(11, new DateDifference(date1, date2).getMonths());

        date2 = new MyDate(7, 7, 2017);
        assertEquals(1, new DateDifference(date1, date2).getMonths());

        date1 = new MyDate(3, 8, 2008);
        date2 = new MyDate(28, 2, 2016);
        assertEquals(6, new DateDifference(date1, date2).getMonths());

        date1 = new MyDate(1, 1, 2008);
        date2 = new MyDate(30, 9, 2025);
        assertEquals(8, new DateDifference(date1, date2).getMonths());

        date1 = new MyDate(28, 2, 2016);
        date2 = new MyDate(28, 2, 2016);
        assertEquals(0, new DateDifference(date1, date2).getYears());
    }

    @Test
    public void testGetDays() {
        MyDate date1 = new MyDate(1, 6, 1934);
        MyDate date2 = new MyDate(7, 5, 2017);

        assertEquals(6, new DateDifference(date1, date2).getDays());

        date1 = new MyDate(10, 6, 1934);
        assertEquals(27, new DateDifference(date1, date2).getDays());

        date1 = new MyDate(4, 4, 1997);
        date2 = new MyDate(4, 2, 1998);
        assertEquals(0, new DateDifference(date1, date2).getDays());

        date1 = new MyDate(3, 8, 2008);
        date2 = new MyDate(29, 2, 2016);
        assertEquals(26, new DateDifference(date1, date2).getDays());

        date1 = new MyDate(1, 1, 2008);
        date2 = new MyDate(30, 9, 2025);
        assertEquals(29, new DateDifference(date1, date2).getDays());

        date1 = new MyDate(28, 2, 2016);
        date2 = new MyDate(28, 2, 2016);
        assertEquals(0, new DateDifference(date1, date2).getYears());
    }
}
