package datecalc;

import datecalc.date.MyDate;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MyDateTest {
    @Test
    public void testGetDaysSinceEpoch() {
        MyDate date1 = new MyDate(3, 8, 2008);
        MyDate date2 = new MyDate(29, 2, 2016);

        assertEquals(2766, date2.getDaysSinceEpoch() - date1.getDaysSinceEpoch());

        date1 = new MyDate(1, 6, 1934);
        date2 = new MyDate(7, 5, 2017);
        assertEquals(30291, date2.getDaysSinceEpoch() - date1.getDaysSinceEpoch());

        date1 = new MyDate(1, 3, 2008);
        date2 = new MyDate(30, 9, 2025);
        assertEquals(6422, date2.getDaysSinceEpoch() - date1.getDaysSinceEpoch());
    }

    @Test
    public void testIsLeapYear() {
        for (int i = 1; i < 200; i++) {
            assertEquals(i % 4 == 0, MyDate.isLeapYear(i));
        }
    }

    @Test
    public void testGetDaysInMonth() {
        int leapYear = 1984;
        int nonLeapYear = 2017;

        for (int month = 1; month <= 7; month += 2) {
            assertEquals(31, MyDate.getDaysInMonth(month, leapYear));
            assertEquals(31, MyDate.getDaysInMonth(month, nonLeapYear));
        }

        for (int month = 8; month <= 12; month += 2) {
            assertEquals(31, MyDate.getDaysInMonth(month, leapYear));
            assertEquals(31, MyDate.getDaysInMonth(month, nonLeapYear));
        }

        for (int month = 4; month <= 6; month += 2) {
            assertEquals(30, MyDate.getDaysInMonth(month, leapYear));
            assertEquals(30, MyDate.getDaysInMonth(month, nonLeapYear));
        }

        for (int month = 9; month <= 11; month += 2) {
            assertEquals(30, MyDate.getDaysInMonth(month, leapYear));
            assertEquals(30, MyDate.getDaysInMonth(month, nonLeapYear));
        }

        assertEquals(29, MyDate.getDaysInMonth(2, leapYear));
        assertEquals(28, MyDate.getDaysInMonth(2, nonLeapYear));
    }

    @Test
    public void testDaysSinceEpochConstructor() {
        for (int i = 100; i < 30000; i++) {
            MyDate date = new MyDate(i);

            assertEquals(i, date.getDaysSinceEpoch());
        }
    }
}
