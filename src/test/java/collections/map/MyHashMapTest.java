package collections.map;

import collections.MyMap;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class MyHashMapTest {
    private Random random = new Random();

    @Test
    public void testPutAndGet() {
        MyHashMap<String, Integer> testMap = new MyHashMap<>();

        for (int i = 0; i < 500; i++) {
            Integer randomValue = random.nextInt();

            testMap.put(randomValue.toString(), randomValue);
            assertEquals(randomValue, testMap.get(randomValue.toString()));
        }
    }

    @Test
    public void testReplaceValue() {
        MyHashMap<String, Integer> testMap = new MyHashMap<>();

        for (int i = 0; i < 500; i++) {
            Integer randomValue = random.nextInt();

            testMap.put(randomValue.toString(), randomValue);

            testMap.put(randomValue.toString(), randomValue + 1);
            assertEquals(randomValue + 1, (long) testMap.get(randomValue.toString()));
        }
    }

    @Test
    public void testPutForNullKey() {
        MyHashMap<String, Integer> testMap = new MyHashMap<>();

        for (int i = 0; i < 500; i++) {
            Integer randomValue = random.nextInt();

            testMap.put(null, randomValue);
            assertEquals((long) randomValue, (long) testMap.get(null));
        }
    }

    @Test
    public void testPutNullValues() {
        MyMap<String, Integer> testMap = new MyHashMap<>();

        for (Integer i = 0; i < 500; i++) {
            testMap.put(i.toString(), null);

            assertNull(testMap.get(i.toString()));
        }
    }

    @Test
    public void testSize() {
        MyMap<Double, Integer> testMap = new MyHashMap<>();

        for (int i = 0; i < 500; i++) {
            assertEquals(i, testMap.size());

            testMap.put(random.nextDouble(), random.nextInt());
        }
    }

    @Test
    public void testIsEmpty() {
        MyMap<Long, Float> testMap = new MyHashMap<>();

        assertTrue(testMap.isEmpty());
        for (int i = 0; i < 500; i++) {
            testMap.put(random.nextLong(), random.nextFloat());
            assertFalse(testMap.isEmpty());
        }
    }

    @Test
    public void testClear() {
        MyMap<Integer, String> testMap = new MyHashMap<>();

        testMap.clear(); //tests invocation on empty set

        for (int i = 0; i < 500; i++) {
            testMap.put(random.nextInt(), Integer.toString(random.nextInt()));
        }
        testMap.clear();

        assertTrue("Set must be empty", testMap.isEmpty());
        assertEquals("Size must be equals to 0", 0, testMap.size());
    }

    @Test
    public void testContainsKey() {
        MyMap<String, Integer> testMap = new MyHashMap<>();

        assertFalse(testMap.containsKey("qwe"));
        for (int i = 0; i < 500; i++) {
            String key = Integer.toString(random.nextInt());
            testMap.put(key, random.nextInt());

            assertTrue(testMap.containsKey(key));
            assertFalse(testMap.containsKey(key + "asd"));
        }

        assertFalse(testMap.containsKey(null));
        testMap.put(null, 0);
        assertTrue(testMap.containsKey(null));
    }

    @Test
    public void testContainsValue() {
        MyMap<Integer, String> testMap = new MyHashMap<>();

        assertFalse(testMap.containsValue("qwe"));
        for (int i = 0; i < 500; i++) {
            String value = Integer.toString(random.nextInt());
            testMap.put(random.nextInt(), value);

            assertTrue(testMap.containsValue(value));
            assertFalse(testMap.containsValue(value + "qweqwe"));
        }

        assertFalse(testMap.containsValue(null));
        testMap.put(12, null);
        assertTrue(testMap.containsValue(null));
    }

    @Test
    public void testRemove() {
        MyMap<String, Integer> testMap = new MyHashMap<>();
        List<Integer> valueList = new ArrayList<>();

        assertNull(testMap.remove("qwe"));
        for (int i = 0; i < 500; i++) {
            Integer value = random.nextInt();

            testMap.put(value.toString(), value);
            valueList.add(value);
        }

        int sizeCounter = 499;
        for (Integer value : valueList) {
            assertEquals("Return value does not match", value, testMap.remove(value.toString()));
            assertEquals("Size changes incorrectly", sizeCounter, testMap.size());
            assertFalse("Result set must not contain removed key", testMap.containsKey(value.toString()));

            sizeCounter--;
        }
    }

    @Test
    public void testRemoveNull() {
        MyMap<String, Integer> testMap = new MyHashMap<>();

        assertNull(testMap.remove(null));

        for (int i = 0; i < 500; i++) {
            testMap.put(Integer.toString(random.nextInt()), random.nextInt());
        }

        assertNull(testMap.remove(null));
        testMap.put(null, 23);
        assertEquals(23, (long) testMap.remove(null));
    }

    @Test
    public void testKeySet() {
        MyMap<Integer, String> testMap = new MyHashMap<>();
        Set<Integer> actualSet = new HashSet<>();

        assertTrue("Key set of empty map must be empty", testMap.keySet().isEmpty());
        for (int i = 0; i < 500; i++) {
            Integer key = random.nextInt();

            testMap.put(key, key.toString());
            actualSet.add(key);
        }

        Set<Integer> returnedSet = testMap.keySet();
        assertEquals("Set sizes mut mathc", actualSet.size(), returnedSet.size());

        for (Integer valInteger : actualSet) {
            assertTrue("Result set must contain all added keys", returnedSet.contains(valInteger));
        }
    }

    @Test
    public void testValues() {
        MyMap<Integer, String> testMap = new MyHashMap<>();
        Collection<String> actualSet = new ArrayList<>();

        assertTrue("Value set of empty map must be empty", testMap.values().isEmpty());
        for (int i = 0; i < 500; i++) {
            Integer key = random.nextInt();

            testMap.put(key, key.toString());
            actualSet.add(key.toString());
        }

        Collection<String> returnedCollection = testMap.values();

        for (String value : actualSet) {
            assertTrue("Result set must contain all added values", returnedCollection.contains(value));
        }
    }

    @Test
    public void testEntrySet() {
        MyMap<Integer, String> testMap = new MyHashMap<>();
        Set<MyEntry<Integer, String>> actualSet = new HashSet<>();

        assertTrue("Key set of empty map must be empty", testMap.entrySet().isEmpty());
        for (int i = 0; i < 500; i++) {
            Integer key = random.nextInt();

            testMap.put(key, key.toString());
            actualSet.add(new MyEntry<>(key, key.toString()));
        }

        Set<MyEntry<Integer, String>> returnedSet = testMap.entrySet();
        assertEquals("Set sizes mut mathc", actualSet.size(), returnedSet.size());

        for (MyEntry<Integer, String> entry : actualSet) {
            assertTrue("Result set must contain all added keys", returnedSet.contains(entry));
        }
    }
}