package collections.list.impl;

import collections.list.MyList;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MyLinkedListTest {
    @Test
    public void testAddAngGet() {
        MyList<Integer> list = new MyLinkedList<>();

        for (int i = 0; i < 11; i++) {
            list.add(i);

            assertEquals((long) i, (long) list.get(i));
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetIndexGreaterThanSize() {
        MyList<Integer> list = new MyLinkedList<>();

        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        System.out.println(list.get(15));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetIndexLessThanZero() {
        MyList<Integer> list = new MyLinkedList<>();

        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        System.out.println(list.get(-5));
    }

    @Test
    public void testClear() {
        MyList<Integer> list = new MyLinkedList<>();

        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        list.clear();
        assertTrue(list.isEmpty());
    }

    @Test
    public void testContains() {
        MyList<Integer> list = new MyLinkedList<>();

        for (int i = 0; i < 15; i++) {
            list.add(i);

            assertTrue(list.contains(i));
        }

        assertFalse(list.contains(102));
    }

    @Test
    public void testIndexOf() {
        MyList<Integer> list = new MyLinkedList<>();

        for (int i = 0; i < 20; i++) {
            list.add(i);

            assertEquals(i, list.indexOf(i));
        }

        assertEquals(-1, list.indexOf(-5));
    }

    @Test
    public void testIsEmpty() {
        MyList<Integer> list = new MyLinkedList<>();

        assertTrue(list.isEmpty());

        list.add(4);
        assertFalse(list.isEmpty());
    }

    @Test
    public void testRemove() {
        MyList<Integer> list = new MyLinkedList<>();

        for (int i = 0; i < 15; i++) {
            list.add(i);
        }

        for (int i = 14; i >= 0; i--) {
            assertEquals("Returns invalid element", i, (long) list.remove(i));
            assertEquals("Size does not changes", i, list.size());
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveIndexGreaterThanSize() {
        MyList<Integer> list = new MyLinkedList<>();

        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        System.out.println(list.remove(15));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveIndexLessThanZero() {
        MyList<Integer> list = new MyLinkedList<>();

        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        System.out.println(list.remove(-5));
    }

    @Test
    public void testSize() {
        MyList<Integer> list = new MyLinkedList<>();

        for (int i = 0; i < 20; i++) {
            assertEquals(i, list.size());
            list.add(i);
        }
    }

    @Test
    public void testSet() {
        MyList<Integer> list = new MyLinkedList<>();

        for (int i = 0; i < 20; i++) {
            list.add(i);

            assertEquals("Returns invalid value", i, (long) list.set(i, i + 15));
            assertEquals("Does not replace value", i + 15, (long) list.get(i));
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSetIndexGreaterThanSize() {
        MyList<Integer> list = new MyLinkedList<>();

        for (int i = 0; i < 15; i++) {
            list.add(i);

            list.set(i + 10, 17);
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSetIndexLessThanZero() {
        MyList<Integer> list = new MyLinkedList<>();

        for (int i = 0; i < 15; i++) {
            list.add(i);

            list.set(-i, 17);
        }
    }

    @Test
    public void testBooleanRemove(){
        MyList<Integer> list = new MyLinkedList<>();

        for(int i=0;i<30;i++){
            Integer element = new Random().nextInt();

            list.add(element);
            assertTrue(list.remove(element));
        }

        assertFalse(list.remove(new Integer(20)));
    }
}