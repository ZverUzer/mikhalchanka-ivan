package collections.list.impl;

import collections.list.MyList;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;

public class MyLinkedListIteratorTest {
    @Test
    public void testHasNext() {
        MyLinkedList<Integer> list = new MyLinkedList<>();
        MyLinkedListIterator<Integer> iterator = new MyLinkedListIterator<>(list);

        assertFalse(iterator.hasNext());

        list.add(5);
        assertTrue(iterator.hasNext());
    }

    @Test
    public void testNext() {
        MyLinkedList<Integer> list = new MyLinkedList<>();
        MyLinkedListIterator<Integer> iterator = new MyLinkedListIterator<>(list);

        list.add(5);
        assertEquals(5, (int) iterator.next());
    }

    @Test
    public void testForeach() {
        MyList<Integer> list = new MyLinkedList<>();

        for (int i = 0; i < 20; i++) {
            list.add(i);
        }

        int counter = 0;
        for (int element : list) {
            assertEquals(counter, element);
            counter++;
        }
        assertEquals(20, counter);

        counter = 0;
        for (int element : list) {
            assertEquals(counter, element);
            counter++;
        }
        assertEquals(20, counter);
    }
}
