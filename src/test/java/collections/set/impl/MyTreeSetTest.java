package collections.set.impl;

import collections.MyCollection;
import collections.list.MyList;
import collections.list.impl.MyLinkedList;
import collections.set.MySet;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class MyTreeSetTest {
    @Test
    public void testAddAndContains() {
        MyCollection<Integer> set = new MyTreeSet<>();
        Random random = new Random();

        for (int i = 0; i < 50; i++) {
            int element = random.nextInt();
            set.add(element);

            assertTrue(set.contains(element));
        }
    }

    @Test
    public void testSize() {
        MyCollection<Integer> set = new MyTreeSet<>();
        Random random = new Random();

        for (int i = 0; i < 50; i++) {
            assertEquals(i, set.size());

            set.add(random.nextInt());
        }
    }

    @Test
    public void testClear() {
        MyCollection<Integer> set = new MyTreeSet<>();
        Random random = new Random();

        set.clear();        //tests clearing of empty set

        for (int i = 0; i < 50; i++) {
            set.add(random.nextInt());
        }

        set.clear();
        assertEquals(0, set.size());
    }

    @Test
    public void testIsEmpty() {
        MyCollection<Integer> set = new MyTreeSet<>();
        Random random = new Random();

        assertTrue(set.isEmpty());

        for (int i = 0; i < 50; i++) {
            set.add(random.nextInt());

            assertFalse(set.isEmpty());
        }
    }

    @Test
    public void testRemove() {
        MySet<Integer> set = new MyTreeSet<>();
        List<Integer> list = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < 50; i++) {
            int value = random.nextInt();
            set.add(value);
            list.add(value);
        }

        for (int value : list) {
            int expectedSize = set.size() - 1;
            assertTrue("Must return 'true', if set contains element", set.remove(value));
            assertEquals("Size must change after element delete", expectedSize, set.size());
            assertFalse("Element didn't remove", set.contains(value));
        }
    }

    @Test
    public void testRetainAll() {
        MyList<Integer> list = new MyLinkedList<>();
        MyList<Integer> equalsList = new MyLinkedList<>();
        MyTreeSet<Integer> treeSet = new MyTreeSet<>();
        Random random = new Random();

        assertFalse("Must return false for empty collections", treeSet.retainAll(list));

        for (int i = -10; i < 30; i++) {
            int value = random.nextInt();

            list.add(value);
            treeSet.add(value);
            equalsList.add(value);
        }

        assertFalse("Must be false for equals collections", treeSet.retainAll(list));

        int numOfEqualsElements = treeSet.size();

        //adding non-equals elements
        for (int i = 0; i < 20; i++) {
            list.add(i + 100);
            treeSet.add(i + 1000);
        }

        assertTrue("Must be true for non-equals collections", treeSet.retainAll(list));
        assertEquals("Result size must be equals to num of equals elements", numOfEqualsElements, treeSet.size());

        for (int value : equalsList) {
            assertTrue("Must contain all equals elements", treeSet.contains(value));
        }
    }
}
