package datecalc.entrypoint;

import datecalc.date.DateDifference;
import datecalc.exceptions.InvalidDateException;
import datecalc.date.MyDate;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        loop(new BufferedReader(new InputStreamReader(System.in)));
    }

    private static void loop(BufferedReader reader) {
        while (true) {
            try {
                System.out.println("Enter two dates using following formats :{dd.mm.yyyy}, {dd/mm/yyyy}");

                MyDate date1 = MyDate.parseMyDate(reader.readLine());
                MyDate date2 = MyDate.parseMyDate(reader.readLine());

                System.out.println(new DateDifference(date1, date2));
                System.out.println();
            } catch (InvalidDateException e) {
                System.out.println(e.toString());
            } catch (IOException e) {
                System.out.println(e.toString());
            }
        }
    }
}
