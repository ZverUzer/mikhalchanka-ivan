package datecalc.date;

import datecalc.exceptions.InvalidDateException;

public final class MyDate implements Comparable<MyDate>, Cloneable {
    private final int day;
    private final int month;
    private final int year;

    public MyDate(int day, int month, int year) {
        validateInputArgs(day, month, year);

        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(int daysSinceEpoch) {
        year = daysSinceEpoch / 365;

        int leapYears = year / 4;
        daysSinceEpoch -= leapYears * 366 + (year - leapYears) * 365;

        if (isLeapYear(year)) {
            daysSinceEpoch += 1;
        }

        int monthNumber = 1;
        while (daysSinceEpoch - getDaysInMonth(monthNumber, year) > 0) {
            daysSinceEpoch -= getDaysInMonth(monthNumber, year);
            monthNumber++;
        }

        month = monthNumber;
        day = daysSinceEpoch;
    }

    private void validateInputArgs(int day, int month, int year) {
        if (year < 0) {
            throw new InvalidDateException("Year must be positive");
        }

        if (month <= 0 || month > 12) {
            throw new InvalidDateException("Month must be in [1;12]");
        }

        if (day <= 0 || day > getDaysInMonth(month, year)) {
            throw new InvalidDateException("Invalid number of days");
        }
    }

    public static boolean isLeapYear(int year) {
        if (year < 0) {
            throw new InvalidDateException("Year must be positive");
        }

        if (year == 0) {
            return false;
        }

        if (year % 4 == 0 && year % 100 != 0) {
            return true;
        }

        if (year % 400 == 0) {
            return true;
        }

        return false;
    }

    public static int getDaysInMonth(int month, int year) {
        if (month < 1 || month > 12) {
            throw new InvalidDateException("Month number must be in [1;12]");
        }

        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 2:
                return (isLeapYear(year)) ? 29 : 28;
            default:
                return 30;
        }
    }

    public static MyDate parseMyDate(String s) {
        String[] values = s.split("(/|\\.)");

        if (values.length != 3) {
            throw new InvalidDateException("Input string must match dd.mm.yyyy or dd/mm/yyyy");
        }

        return new MyDate(Integer.parseInt(values[0]), Integer.parseInt(values[1]), Integer.parseInt(values[2]));
    }

    public int getDaysSinceEpoch() {
        int numOfLeapYears = (year) / 4;
        int result = numOfLeapYears * 366 + ((year) - numOfLeapYears) * 365;

        for (int i = 1; i < month; i++) {
            result += getDaysInMonth(i, year);
        }

        if (isLeapYear(year)) {
            result--;
        }

        return result + day;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    @Override
    public MyDate clone() {
        return new MyDate(day, month, year);
    }

    @Override
    public int compareTo(MyDate o) {
        if (year != o.getYear()) {
            return year - o.getYear();
        }

        if (month != o.getMonth()) {
            return month - o.getMonth();
        }

        return day - o.getDay();
    }

    @Override
    public String toString() {
        return "MyDate{" +
                "day=" + day +
                ", month=" + month +
                ", year=" + year +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyDate myDate = (MyDate) o;

        if (day != myDate.day) return false;
        if (month != myDate.month) return false;
        return year == myDate.year;

    }

    @Override
    public int hashCode() {
        return getDaysSinceEpoch();
    }
}