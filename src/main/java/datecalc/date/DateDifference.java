package datecalc.date;

public final class DateDifference {
    private final MyDate smallerDate;
    private final MyDate biggerDate;

    private final int daysDifferenceCache;

    public DateDifference(MyDate firstDate, MyDate secondDate) {
        if (firstDate.compareTo(secondDate) < 0) {
            smallerDate = firstDate.clone();
            biggerDate = secondDate.clone();
        } else {
            smallerDate = secondDate.clone();
            biggerDate = firstDate.clone();
        }

        daysDifferenceCache = biggerDate.getDaysSinceEpoch() - smallerDate.getDaysSinceEpoch();
    }

    public MyDate getSmallerDate() {
        return smallerDate.clone();
    }

    public MyDate getBiggerDate() {
        return biggerDate.clone();
    }

    public int getYears() {
        int years = biggerDate.getYear() - smallerDate.getYear();

        if (smallerDate.getMonth() > biggerDate.getMonth() && years > 0) {
            years--;
        }

        return years;
    }

    public int getMonths() {
        int leapYearsInPeriod = biggerDate.getYear() / 4 - smallerDate.getYear() / 4;
        int nonLeapYearsInPeriod = getYears() - leapYearsInPeriod;

        int daysDifference = daysDifferenceCache - (leapYearsInPeriod * 366 + nonLeapYearsInPeriod * 365);

        if (daysDifference == 30) {
            return 1;
        }

        return (int) (daysDifference / 30.4167);
    }

    public int getDays() {
        int daysDifference = daysDifferenceCache;

        int monthNo = smallerDate.getMonth();
        int yearNo = smallerDate.getYear();

        while (monthNo < biggerDate.getMonth() || yearNo < biggerDate.getYear()) {
            daysDifference -= MyDate.getDaysInMonth(monthNo, yearNo);

            monthNo++;
            if (monthNo == 13) {
                monthNo = 1;
                yearNo++;
            }
        }

        if (biggerDate.getDay() < smallerDate.getDay()) {
            daysDifference = MyDate.getDaysInMonth(smallerDate.getMonth(), smallerDate.getYear()) + daysDifference;
        }

        return daysDifference;
    }

    @Override
    public String toString() {
        return "DateDifference {" +
                "years=" + getYears() +
                ", months=" + getMonths() +
                ", days=" + getDays() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DateDifference that = (DateDifference) o;

        if (getYears() != that.getYears()) return false;
        if (getMonths() != that.getMonths()) return false;
        return getDays() == that.getDays();

    }

    @Override
    public int hashCode() {
        int result = getYears();
        result = 31 * result + getMonths();
        result = 31 * result + getDays();
        return result;
    }
}