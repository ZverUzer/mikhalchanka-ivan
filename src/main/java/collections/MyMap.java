package collections;

import collections.map.MyEntry;

import java.util.Collection;
import java.util.Set;

public interface MyMap<K, V> {
    /**
     * Возвращает размер отображения
     *
     * @return количество элементов, содержащихся в отображении
     */
    int size();

    /**
     * Проверяет отображение на наличие в нём элементов
     *
     * @return true если в отображении нет элементов
     */
    boolean isEmpty();

    /**
     * Проверяет отображение на наличие в нём переданного ключа
     * @param key ключ, наличие которого надо проверить
     * @return true, если данный ключ присутствует в отображении
     */
    boolean containsKey(Object key);

    /**
     * Проверяет отображение на наличие в нём заданного значения
     *
     * @param value значение, наличие которого надо проверить
     * @return true, если отображение содержит переданное значение
     */
    boolean containsValue(Object value);

    /**
     * Возвращает значение, на которое отображёт переданный ключ
     *
     * @param key ключ, с которым проассоциировано значение
     * @return значение, с которым ассоциируется переданный ключ
     * null, если переданный ключ не ассоциируется ни с каким значением
     */
    V get(Object key);

    /**
     * Ассоциирует переданный ключ с переданным значением
     *
     * @param key ключ, который необходимо проассоциировать со значением
     * @param value значение, которое должно быть проассоциировано с ключом
     * @return предыдущее значение, которое было проассоциировано с переданным ключом
     * null, если переданный ключ не ассоциируется ни с каким значением
     */
    V put(K key, V value);

    /**
     *Удаляет пару ключ-значение по переданному ключу
     *
     * @param key ключ, по которому необходимо удалить пару ключ-значение
     * @return значение, с которым был проассоциирован переданный ключ
     */
    V remove(Object key);

    /**
     * Удаляет все ппары ключ-значение, содержащиеся в отображении
     */
    void clear();

    /**
     * Возвращает набор ключей, содержащихся в отображении
     *
     * @return набор ключей, содержащихся в отображении
     */
    Set<K> keySet();

    /**
     * Возвращает множество значений, содержащихся в отображении
     *
     * @return множество значений, содержащихся в отображении
     */
    Collection<V> values();

    /**
     * Возвращает {@link MySet} пар ключ-значение, содержащихся в отображении
     * Пары передаются по ссылке без использования клонировани,
     * поэтому изменения в переданном наборе будут влиять также и на
     * исходное отображение. Верно и обратное.
     *
     * @return набор пар ключ-значение, содержащихся в отображении
     */
    Set<MyEntry<K, V>> entrySet();
}