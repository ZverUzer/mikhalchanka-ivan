package collections.map;

public class MyEntry<K, V> {
    private K key;
    private V value;
    private MyEntry<K, V> nextEntry;

    public MyEntry(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public MyEntry<K, V> getNextEntry() {
        return nextEntry;
    }

    public boolean hasNext() {
        return nextEntry != null;
    }

    public int getHash() {
        return key.hashCode();
    }

    public void setNextEntry(MyEntry<K, V> nextEntry) {
        this.nextEntry = nextEntry;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyEntry<?, ?> myEntry = (MyEntry<?, ?>) o;

        if (key != null ? !key.equals(myEntry.key) : myEntry.key != null) return false;
        return value != null ? value.equals(myEntry.value) : myEntry.value == null;

    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}