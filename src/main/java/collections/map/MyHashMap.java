package collections.map;

import collections.MyMap;

import java.util.*;

public class MyHashMap<K, V> implements MyMap<K, V> {
    public final int DEFAULT_NUM_OF_BUCKETS = 16;
    public final double DEFAULT_LOAD_FACTOR = 0.75;

    private MyEntry<K, V>[] buckets;
    private int size = 0;
    private double loadFactor;

    public MyHashMap() {
        buckets = new MyEntry[DEFAULT_NUM_OF_BUCKETS];
        loadFactor = DEFAULT_LOAD_FACTOR;
    }

    private int calcBucketIndex(int hashCode, int size) {
        return hashCode & (size - 1);
    }

    private double calcCurrentLoadFactor() {
        int loadedBuckets = 0;
        for (MyEntry bucket : buckets) {
            if (bucket != null) {
                loadedBuckets++;
            }
        }

        return (double) loadedBuckets / (double) buckets.length;
    }

    private void resize(int size) {
        MyEntry<K, V>[] newBuckets = new MyEntry[size];

        transfer(newBuckets);

        buckets = newBuckets;
    }

    private void transfer(MyEntry<K, V>[] newBuckets) {
        for (MyEntry<K, V> bucket : buckets) {
            MyEntry<K, V> entry = bucket;

            while (entry != null) {
                int bucketIndex = calcBucketIndex(entry.getHash(), newBuckets.length);

                if (newBuckets[bucketIndex] == null) {
                    newBuckets[bucketIndex] = new MyEntry<K, V>(entry.getKey(), entry.getValue());
                } else {
                    insertIntoBucket(newBuckets[bucketIndex], new MyEntry<K, V>(entry.getKey(), entry.getValue()));
                }

                entry = entry.getNextEntry();
            }
        }
    }

    private V insertIntoBucket(MyEntry<K, V> bucket, MyEntry<K, V> entry) {
        MyEntry<K, V> current = bucket;

        while (true) {
            if (current.getKey().equals(entry.getKey())) {
                V prevValue = current.getValue();
                current.setValue(entry.getValue());

                return prevValue;
            }

            if (!current.hasNext()) {
                current.setNextEntry(entry);

                return null;
            }

            current = current.getNextEntry();
        }
    }

    private V putForNullKey(V value) {
        if (buckets[0] == null) {
            buckets[0] = new MyEntry<K, V>(null, value);

            return null;
        }

        MyEntry<K, V> current = buckets[0];

        while (true) {
            if (current.getKey() == null) {
                V previous = current.getValue();
                current.setValue(value);

                return previous;
            }

            if (!current.hasNext()) {
                current.setNextEntry(new MyEntry<K, V>(null, value));

                return null;
            }

            current = current.getNextEntry();
        }
    }

    private V getNullKeyValue() {
        MyEntry<K, V> current = buckets[0];

        while (current != null) {
            if (current.getKey() == null) {
                return current.getValue();
            }

            current = current.getNextEntry();
        }

        return null;
    }

    private V removeNullKey() {
        MyEntry<K, V> current = buckets[0];
        MyEntry<K, V> previous = buckets[0];

        while (current != null) {
            if (current.getKey() == null) {
                V value = current.getValue();

                if (buckets[0].getKey() == null) {
                    buckets[0] = buckets[0].getNextEntry();
                } else {
                    previous.setNextEntry(current.getNextEntry());
                }

                return value;
            }

            current = current.getNextEntry();
        }

        return null;
    }

    private boolean containsNullKey() {
        MyEntry<K, V> current = buckets[0];

        while (current != null) {
            if (current.getKey() == null) {
                return true;
            }

            current = current.getNextEntry();
        }

        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        if (key != null) {
            int bucketNum = calcBucketIndex(key.hashCode(), buckets.length);

            if (buckets[bucketNum] != null) {
                MyEntry<K, V> current = buckets[bucketNum];

                while (current != null) {
                    if (current.getKey().equals(key)) {
                        return true;
                    }

                    current = current.getNextEntry();
                }
            }
        } else {
            return containsNullKey();
        }

        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        for (MyEntry<K, V> bucket : buckets) {
            if (bucket != null) {
                MyEntry<K, V> current = bucket;

                while (current != null) {
                    if (current.getValue() != null && current.getValue().equals(value)) {
                        return true;
                    } else if (current.getValue() == null && value == null) {
                        return true;
                    }

                    current = current.getNextEntry();
                }
            }
        }

        return false;
    }

    @Override
    public V get(Object key) {
        if (key == null) {
            return getNullKeyValue();
        }

        int bucketNum = calcBucketIndex(key.hashCode(), buckets.length);

        if (buckets[bucketNum] != null) {
            MyEntry<K, V> current = buckets[bucketNum];

            while (current != null) {
                if (current.getKey().equals(key)) {
                    return current.getValue();
                }

                current = current.getNextEntry();
            }
        }

        return null;
    }

    @Override
    public V put(K key, V value) {
        if (key == null) {
            return putForNullKey(value);
        }

        MyEntry<K, V> entry = new MyEntry<>(key, value);

        V previousValue = null;

        int bucketIndex = calcBucketIndex(entry.getHash(), buckets.length);
        if (buckets[bucketIndex] == null) {
            buckets[bucketIndex] = entry;
        } else {
            previousValue = insertIntoBucket(buckets[bucketIndex], entry);
        }

        if (calcCurrentLoadFactor() >= loadFactor) {
            resize(buckets.length * 2);
        }

        if (previousValue == null) {
            size++;
        }

        return previousValue;
    }

    @Override
    public V remove(Object key) {
        if (key == null) {
            return removeNullKey();
        }

        int bucketIndex = calcBucketIndex(key.hashCode(), buckets.length);
        MyEntry<K, V> current = buckets[bucketIndex];
        MyEntry<K, V> previous = buckets[bucketIndex];
        V value = null;

        while (current != null) {
            if (current.getKey().equals(key)) {
                value = current.getValue();

                if (current.getKey().equals(buckets[bucketIndex].getKey())) {
                    buckets[bucketIndex] = buckets[bucketIndex].getNextEntry();
                } else {
                    previous.setNextEntry(current.getNextEntry());
                }

                size--;
                return value;
            }

            current = current.getNextEntry();
        }

        return null;
    }

    @Override
    public void clear() {
        buckets = new MyEntry[DEFAULT_NUM_OF_BUCKETS];
        size = 0;
    }

    @Override
    public Set<K> keySet() {
        Set<K> keySet = new HashSet<>();

        for (MyEntry<K, V> bucket : buckets) {
            MyEntry<K, V> entry = bucket;

            while (entry != null) {
                keySet.add(entry.getKey());
                entry = entry.getNextEntry();
            }
        }

        return keySet;
    }

    @Override
    public Collection<V> values() {
        Collection<V> values = new ArrayList<>();

        for (MyEntry<K, V> bucket : buckets) {
            MyEntry<K, V> entry = bucket;

            while (entry != null) {
                values.add(entry.getValue());
                entry = entry.getNextEntry();
            }
        }

        return values;
    }

    @Override
    public Set<MyEntry<K, V>> entrySet() {
        Set<MyEntry<K, V>> entrySet = new HashSet<>();

        for (MyEntry<K, V> bucket : buckets) {
            MyEntry<K, V> entry = bucket;

            while (entry != null) {
                entrySet.add(new MyEntry<K, V>(entry.getKey(), entry.getValue()));
                entry = entry.getNextEntry();
            }
        }

        return entrySet;
    }
}