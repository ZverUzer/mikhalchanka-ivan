package collections.set;

import collections.MyCollection;

public interface MySet<T> extends MyCollection<T> {
    /**
     * Метод оставляет в множестве только те элементы, которые были переданы ему в коллекции.
     * Другими словами, удалает из множества элементы, которых не было в переданной ему коллекции.
     *
     * @param collection коллекция элементов, которые необходимо сохранить в множестве.
     * @return true если множество было изменено в процессе выполнения метода, false в противном случае.
     */
    boolean retainAll(MyCollection<T> collection);
}
