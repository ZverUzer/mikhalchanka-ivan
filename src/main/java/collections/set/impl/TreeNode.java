package collections.set.impl;

public class TreeNode<T> {
    private T value;
    private TreeNode<T> leftChild;
    private TreeNode<T> rightChild;
    private boolean isRed;

    public TreeNode(T value) {
        this.value = value;
        isRed = true;
    }

    public void flipColors() {
        isRed = true;

        if (leftChild != null) {
            leftChild.setRed(false);
        }
        if (rightChild != null) {
            rightChild.setRed(false);
        }
    }

    public boolean isLeftRed() {
        return leftChild != null && leftChild.isRed();
    }

    public boolean isRightRed() {
        return rightChild != null && rightChild.isRed();
    }

    public boolean isRed() {
        return isRed;
    }

    public void setRed(boolean red) {
        isRed = red;
    }

    public boolean haveLeftChild() {
        return leftChild != null;
    }

    public boolean haveRightChild() {
        return rightChild != null;
    }

    public void replaceChild(TreeNode<T> existingChild, TreeNode<T> newChild) {
        if (existingChild.equals(leftChild)) {
            leftChild = newChild;
        }
        if (existingChild.equals(rightChild)) {
            rightChild = newChild;
        }
    }

    public TreeNode<T> getSuccessor() {
        TreeNode<T> current = rightChild;
        TreeNode<T> parent = rightChild;

        while (current.haveLeftChild()) {
            parent = current;
            current = current.getLeftChild();
        }

        if (!current.equals(rightChild) && current.haveRightChild()) {
            parent.setLeftChild(current.getRightChild());
        } else {
            parent.setLeftChild(null);
        }

        return current;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public TreeNode<T> getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(TreeNode<T> leftChild) {
        this.leftChild = leftChild;
    }

    public TreeNode<T> getRightChild() {
        return rightChild;
    }

    public void setRightChild(TreeNode<T> rightChild) {
        this.rightChild = rightChild;
    }

    @Override
    public String toString() {
        return ((isRed) ? "R" : "B") + value.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TreeNode<?> treeNode = (TreeNode<?>) o;

        return value.equals(treeNode.value);

    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
