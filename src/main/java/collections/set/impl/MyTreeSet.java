package collections.set.impl;

import collections.MyCollection;
import collections.list.MyList;
import collections.list.impl.MyLinkedList;
import collections.set.MySet;

import java.util.Comparator;
import java.util.Stack;

public class MyTreeSet<T> implements MySet<T> {
    private TreeNode<T> headNode;
    private Comparator<T> comparator;

    public MyTreeSet() {
    }

    public MyTreeSet(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    private void initComparator(T element) {
        if (element instanceof Comparable) {
            comparator = (T o1, T o2) -> ((Comparable) o1).compareTo(o2);
        } else {
            comparator = (T o1, T o2) -> o1.hashCode() - o2.hashCode();
        }
    }

    private TreeNode<T> rotateLeft(TreeNode<T> node) {
        TreeNode<T> newParentNode = node.getRightChild();

        node.setRightChild(newParentNode.getLeftChild());
        newParentNode.setLeftChild(node);

        newParentNode.setRed(node.isRed());
        node.setRed(true);

        return newParentNode;
    }

    private TreeNode<T> rotateRight(TreeNode<T> node) {
        TreeNode<T> newParentNode = node.getLeftChild();

        node.setLeftChild(newParentNode.getRightChild());
        newParentNode.setRightChild(node);

        newParentNode.setRed(node.isRed());
        node.setRed(true);

        return newParentNode;
    }

    @Override
    public void add(T element) {
        if (comparator == null) {
            initComparator(element);
        }

        headNode = addRecursive(headNode, element);
    }

    private TreeNode<T> addRecursive(TreeNode<T> node, T value) {
        if (node == null) {
            return new TreeNode<>(value);
        }

        if (comparator.compare(node.getValue(), value) > 0) {
            node.setLeftChild(addRecursive(node.getLeftChild(), value));
        } else {
            node.setRightChild(addRecursive(node.getRightChild(), value));
        }

        if (node.isRightRed() && !node.isLeftRed()) {
            node = rotateLeft(node);
        }
        if (node.getLeftChild() != null && node.isLeftRed() && node.getLeftChild().isLeftRed()) {
            node = rotateRight(node);
        }
        if (node.isLeftRed() && node.isRightRed()) {
            node.flipColors();
        }

        return node;
    }

    @Override
    public void clear() {
        headNode = null;
    }

    @Override
    public boolean contains(T element) {
        if (headNode != null) {
            TreeNode<T> current = headNode;

            while (current != null) {
                if (comparator.compare(current.getValue(), element) == 0) {
                    return true;
                }

                if (comparator.compare(current.getValue(), element) > 0) {
                    current = current.getLeftChild();
                } else {
                    current = current.getRightChild();
                }
            }
        }

        return false;
    }

    @Override
    public boolean isEmpty() {
        return headNode == null;
    }

    @Override
    public boolean remove(T element) {
        if (contains(element)) {
            TreeNode<T> parent = headNode;
            TreeNode<T> current = headNode;
            while (comparator.compare(current.getValue(), element) != 0) {
                parent = current;

                if (comparator.compare(current.getValue(), element) > 0) {
                    current = current.getLeftChild();
                } else {
                    current = current.getRightChild();
                }
            }

            if (!(current.haveRightChild() && current.haveLeftChild())) {
                if (current.equals(headNode)) {
                    if (current.haveLeftChild()) {
                        headNode = current.getLeftChild();
                    } else {
                        headNode = current.getRightChild();
                    }
                } else if (current.haveLeftChild()) {
                    parent.replaceChild(current, current.getLeftChild());
                } else {
                    parent.replaceChild(current, current.getRightChild());
                }
            } else {
                TreeNode<T> successor = current.getSuccessor();
                successor.setLeftChild(current.getLeftChild());
                if (!successor.equals(current.getRightChild())) {
                    successor.setRightChild(current.getRightChild());
                }

                if (current.equals(headNode)) {
                    headNode = successor;
                } else {
                    parent.replaceChild(current, successor);
                }
            }

            return true;
        }

        return false;
    }

    public void printTreeToConsole() {
        printRecursive(headNode, 0);
    }

    private void printRecursive(TreeNode<T> node, int numOfSpaces) {
        if (node != null) {
            printRecursive(node.getLeftChild(), ++numOfSpaces);

            numOfSpaces--;
            StringBuilder builder = new StringBuilder("");
            for (int i = 0; i < numOfSpaces * 4; i++) {
                builder.append(" ");
            }
            System.out.println(builder.toString() + node);

            printRecursive(node.getRightChild(), ++numOfSpaces);
        }
    }

    @Override
    public int size() {
        int counter = 0;

        Stack<TreeNode<T>> stack = new Stack<>();
        if (headNode != null) {
            stack.push(headNode);
        }

        while (!stack.isEmpty()) {
            counter++;

            TreeNode<T> node = stack.pop();

            if (node.getLeftChild() != null) {
                stack.push(node.getLeftChild());
            }
            if (node.getRightChild() != null) {
                stack.push(node.getRightChild());
            }
        }

        return counter;
    }

    @Override
    public boolean retainAll(MyCollection<T> collection) {
        MyList<T> deleteList = new MyLinkedList<>();
        Stack<TreeNode<T>> stack = new Stack<>();

        if (headNode != null) {
            stack.push(headNode);
        }
        while (!stack.isEmpty()) {
            TreeNode<T> currentNode = stack.pop();
            if (!collection.contains(currentNode.getValue())) {
                deleteList.add(currentNode.getValue());
            }

            if (currentNode.haveLeftChild()) {
                stack.push(currentNode.getLeftChild());
            }
            if (currentNode.haveRightChild()) {
                stack.push(currentNode.getRightChild());
            }
        }

        for (T element : deleteList) {
            remove(element);
        }

        return !deleteList.isEmpty();
    }
}
