package collections;

public interface MyCollection<T> {
    /**
     * +     * Добавляет элемент в конец коллекции.
     * +     * @param element элемент, который необходимо добавить.
     * +
     */
    void add(T element);

    /**
     * Очищает коллекцию.
     */
    void clear();

    /**
     * Проверяет, содержится ли уже элемент в коллекции.
     *
     * @param element элемент, который необходимо проверить.
     * @return true если элемент содержится в коллекции, false если элемента нет.
     */
    boolean contains(T element);

    /**
     * Проверяет коллекцию на наличие элементов в ней.
     *
     * @return true если коллекция пуста, false если в коллекции содержится хотябы один элемент.
     */
    boolean isEmpty();

    /**
     * Удаляет элемент из коллекции.
     *
     * @param element элемент, который необходимо удалить.
     * @return true если элемент присутствовал в коллекции и был удалён, иначе - false.
     */
    boolean remove(T element);

    /**
     * Возвращает размер коллекции.
     *
     * @return количество элементов, содержащееся в коллекции.
     */
    int size();
}
