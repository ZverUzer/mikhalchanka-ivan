package collections.list;

import collections.MyCollection;

public interface MyList<T> extends Iterable<T>, MyCollection<T> {
    /**
     * Возвращает элемент по его индексу.
     *
     * @param elementIndex индекс элемента.
     * @return элемент, найденный на заданной позиции списка.
     */
    T get(int elementIndex);

    /**
     * Возвращает индекс первого найденного элемента с заданным значением.
     *
     * @param element значение для поиска.
     * @return индекс первого вхождения элемента.
     */
    int indexOf(T element);

    /**
     * Удаляет элемент из списка и возвращает значение этого элемента.
     *
     * @param elementIndex индекс элемента, который необходимо удалить.
     * @return значение удалённого элемента.
     */
    T remove(int elementIndex);

    /**
     * Устанавливает заданное значение элементу на заданной позиции.
     *
     * @param index   позиция, куда нужно вставить новое значение.
     * @param element значение, которое необходимо вставить на новую позицию.
     * @return предыдущее значение элмента, находящегося на заданной позиции.
     */
    T set(int index, T element);
}