package collections.list.impl;

import collections.list.MyList;

import java.util.*;

public class MyLinkedList<T> implements MyList<T> {
    private Entry<T> headNode;
    private Entry<T> tailNode;

    @Override
    public void add(T element) {
        Entry entry = new Entry(element);

        if (headNode == null) {
            headNode = entry;
            tailNode = entry;
        } else {
            tailNode.setNextNode(entry);
            entry.setPreviousNode(tailNode);
            tailNode = entry;
        }
    }

    @Override
    public void clear() {
        headNode = null;
        tailNode = null;
    }

    @Override
    public boolean contains(T element) {
        Entry<T> current = headNode;

        while (current != null) {
            if (current.getValue().equals(element)) {
                return true;
            }

            current = current.getNextNode();
        }

        return false;
    }

    @Override
    public T get(int index) {
        if (index >= size() || index < 0) {
            throw new IndexOutOfBoundsException();
        }

        Entry<T> currentNode = headNode;
        for (int i = 0; i < index; i++) {
            currentNode = currentNode.getNextNode();
        }

        return currentNode.getValue();
    }

    @Override
    public int indexOf(T element) {
        int index = 0;

        Entry<T> current = headNode;
        while (current != null) {
            if (current.getValue().equals(element)) {
                return index;
            }

            index++;
            current = current.getNextNode();
        }

        return -1;
    }

    @Override
    public boolean isEmpty() {
        return headNode == null;
    }

    @Override
    public T remove(int elementIndex) {
        if (elementIndex < 0 || elementIndex >= size()) {
            throw new IndexOutOfBoundsException();
        }

        Entry<T> current = headNode;
        for (int i = 0; i < elementIndex; i++) {
            current = current.getNextNode();
        }

        if (current.hasPrevious()) {
            Entry<T> previous = current.getPreviousNode();
            previous.setNextNode(current.getNextNode());

            if (current.hasNext()) {
                current.getNextNode().setPreviousNode(previous);
            }
        } else {
            headNode = current.getNextNode();

            if (headNode == null) {
                tailNode = null;
            }
        }

        return current.getValue();
    }

    @Override
    public int size() {
        int counter = 0;

        Entry<T> currentNode = headNode;
        while (currentNode != null) {
            counter++;
            currentNode = currentNode.getNextNode();
        }

        return counter;
    }

    @Override
    public T set(int index, T element) {
        if (index >= size() || index < 0) {
            throw new IndexOutOfBoundsException();
        }

        Entry<T> current = headNode;
        for (int i = 0; i < index; i++) {
            current = current.getNextNode();
        }

        T value = current.getValue();
        current.setValue(element);

        return value;
    }

    @Override
    public boolean remove(T element) {
        int elementIndex = indexOf(element);

        if (elementIndex == -1) {
            return false;
        } else {
            remove(elementIndex);

            return true;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new MyLinkedListIterator<T>(this);
    }
}
