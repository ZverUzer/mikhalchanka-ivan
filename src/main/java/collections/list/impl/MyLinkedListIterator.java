package collections.list.impl;

import java.util.Iterator;

public class MyLinkedListIterator<T> implements Iterator<T> {
    private MyLinkedList<T> list;

    private int counter;

    public MyLinkedListIterator(MyLinkedList<T> list) {
        this.list = list;
    }

    @Override
    public boolean hasNext() {
        return counter < list.size();
    }

    @Override
    public T next() {
        return list.get(counter++);
    }
}
