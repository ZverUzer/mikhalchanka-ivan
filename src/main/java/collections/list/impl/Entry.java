package collections.list.impl;

public class Entry<T> {
    private T value;
    private Entry<T> nextNode;
    private Entry<T> previousNode;

    public Entry(T value) {
        this.value = value;
    }

    public boolean hasNext() {
        return nextNode != null;
    }

    public boolean hasPrevious() {
        return previousNode != null;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Entry<T> getNextNode() {
        return nextNode;
    }

    public void setNextNode(Entry<T> nextNode) {
        this.nextNode = nextNode;
    }

    public Entry<T> getPreviousNode() {
        return previousNode;
    }

    public void setPreviousNode(Entry<T> previousNode) {
        this.previousNode = previousNode;
    }
}
