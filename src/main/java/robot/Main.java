package robot;

import java.awt.*;

public class Main {
    private static GameBot gameBot;

    public static void main(String[] args) throws AWTException, InterruptedException {
        System.out.println("Start");
        int x = 307, y = 398, radius = 150;
        gameBot = new GameBot(x, y, 0);

        mark(x, y, radius);
    }

    public static void mark(int x, int y, int radius) throws InterruptedException {
        gameBot.markSquare(x, y, radius);
        if (gameBot.isNearBlackPixel(4) && radius > 2) {
            mark(x + radius, y - radius, radius / 2);
            mark(x - radius, y + radius, radius / 2);
            mark(x + radius, y + radius, radius / 2);
            mark(x - radius, y - radius, radius / 2);
        }
    }
}
