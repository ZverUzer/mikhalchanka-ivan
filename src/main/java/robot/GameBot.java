package robot;

import java.awt.*;

public class GameBot {
    private Robot robot = new Robot();
    private int mouseX;
    private int mouseY;

    private final int DX = 1;
    private final int DY = 1;
    private final int MOVE_DELAY;

    public GameBot(int mouseX, int mouseY, int moveDelay) throws AWTException {
        this.mouseX = mouseX;
        this.mouseY = mouseY;
        MOVE_DELAY = moveDelay;

        robot.delay(5000);
        robot.mouseMove(mouseX, mouseY);
    }

    public void markSquare(int centerX, int centerY, int radius) {
        moveCursorSlowly(centerX - radius, centerY - radius);
        moveCursorSlowly(centerX - radius, centerY + radius);
        moveCursorSlowly(centerX + radius, centerY + radius);
        moveCursorSlowly(centerX + radius, centerY - radius);

        moveCursorSlowly(centerX, centerY);
    }

    public boolean isNearBlackPixel(int searchRadius) {
        for (int i = mouseX - searchRadius; i < mouseX + searchRadius; i++) {
            for (int j = mouseY - searchRadius; j < mouseY + searchRadius; j++) {
                if (robot.getPixelColor(i, j).equals(Color.BLACK)) {
                    return true;
                }
            }
        }

        return false;
    }

    public void moveCursorSlowly(int x, int y) {
        while (x != mouseX || y != mouseY) {
            if (x > mouseX) {
                mouseX += DX;
            } else if (x < mouseX) {
                mouseX -= DX;
            }

            if (y > mouseY) {
                mouseY += DY;
            } else if (y < mouseY) {
                mouseY -= DY;
            }

            robot.mouseMove(mouseX, mouseY);
            robot.delay(MOVE_DELAY);
        }
    }
}
